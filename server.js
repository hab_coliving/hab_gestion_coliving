require('dotenv').config();

const bodyParser = require('body-parser');
const bcrypt = require('bcrypt');
const config = require('config');
const express = require('express');
const moment = require('moment');
const randtoken = require('rand-token');
const { createLogger, format, transports } = require('winston');
const { combine, timestamp, label, printf } = format;
const { setup } = require('axios-cache-adapter');
const sgMail = require('@sendgrid/mail');
const Joi = require('@hapi/joi');
const jsonwebtoken = require('jsonwebtoken');
const cors = require("cors");

const port = process.env.PORT || 3300;
const api = setup({
    baseURL: ` http://localhost:${port}`,
    cache: {
        maxAge: 15 * 60 * 1000
    }
});
const db = require('./db');

const app = express();
app.use(cors({
    origin: '*',
    optionsSuccessStatua: 200
}));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

//domainCors = config.get('domainCors');
/*app.use(function (req, res, next) {
    res.header("Access-Control-Allow-Origin", '*');
    res.header ("Access-Control-Allow-Methods", "GET,PUT,POST,DELETE,OPTIONS");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, Authorization");
    next();
});/*/

// Winston config
const myFormat = printf(({ level, message, label, timestamp }) => {
    return `${timestamp} [${label}] ${level}: ${message}`;
});
const logger = createLogger({
    level: 'debug',
    format: combine(
        label({ label: 'main' }), timestamp(),
        myFormat
    ),
    defaultMeta: { service: 'user-service' },
    transports: [
        new transports.File({ filename: 'debug.log', level: 'debug' })
    ]
});

app.use((req, res, next) => {
    logger.debug(`Request to ${req.url} with method ${req.method} and body data ${JSON.stringify(req.body)}`);

    next();
});

// Portada
app.get('/', (request, response) => {
    res.send('llega peticion: ')
});

// Registro de colivers
app.post('/signup', async (request, response) => {
    const name = request.body.name;
    const surname = request.body.surname;
    const email = request.body.email;
    const password = request.body.password;
    const profession = request.body.profession;
    const country = request.body.country;

    const schema = Joi.object({
        name: Joi.string()
            .required(),
        surname: Joi.string()
            .required(),
        password: Joi.string()
            .required(),
        email: Joi.string()
            .email()
            .required(),
        profession: Joi.string()
            .required(),
        country: Joi.string()
            .required()
    });

    try {
        await schema.validateAsync(request.body);

        const BCRYPT_SALT_ROUNDS = 12;
        const hashedPassword = await bcrypt.hash(password, BCRYPT_SALT_ROUNDS);
        const id = await db.saveUser(name, surname, email, hashedPassword, profession, country);

        const token = jsonwebtoken.sign({
            id: id,
            role: 'coliver'
        }, config.token_password, {
            expiresIn: 86400
        });

        response.send({ token, id, name, surname, email, profession, country });
    } catch (e) {
        console.log(e.message);
        response.status(400).send(e);
    }
});

// Login colivers
app.post('/login', async (request, response) => {
    try {
        const email = request.body.email;
        const password = request.body.password;
        const user = await db.getUserByEmail(email);
        const id = user.id;
        const name = user.name;
        const surname = user.surname;
        const profession = user.profession;
        const country = user.country;

        const samePassword = await bcrypt.compare(password, user.password);
        if (!samePassword) {
            return response.status(403).send('Usuario o password incorrectos');
        }

        const token = jsonwebtoken.sign({
            id: id,
            role: 'coliver'
        }, config.token_password, {
            expiresIn: 86400
        });
        response.json({ token, id, name, surname, email, profession, country });
    } catch (e) {
        console.log(e.message);
        response.status(403).send({});
    }
});

// Registro de gestores
app.post('/host/signup', async (request, response) => {
    const contactName = request.body.name;
    const surname = request.body.surname;
    const email = request.body.email;
    const password = request.body.password;
    const colivingName = request.body.colivingName;
    const street = request.body.street;
    const zipcode = request.body.zipcode;
    const city = request.body.city;
    const country = request.body.country;
    const apartments = request.body.apartments;
    const rooms_count = request.body.rooms_count;
    const roomType = request.body.roomType;
    const facilities = request.body.facilities;
    const website = request.body.website;
    const facebook = request.body.facebook;
    const instagram = request.body.instagram;
    const twitter = request.body.twitter;
    const photo_room1 = request.body.photo_room1;
    const weekly_price = request.body.weekly_price;
    const monthly_price = request.body.monthly_price;
    const description = request.body.description;
    const schema = Joi.object({
        name: Joi.string()
            .required(),
        surname: Joi.string()
            .required(),
        password: Joi.string()
            .required(),
        email: Joi.string()
            .email()
            .required(),
        colivingName: Joi.string()
            .required(),
        apartments: Joi.number()
            .required(),
        facilities: Joi.array(),
        rooms_count: Joi.number()
            .required(),
        roomType: Joi.string()
            .required(),
        street: Joi.string()
            .required(),
        zipcode: Joi.string()
            .required(),
        city: Joi.string()
            .required(),
        country: Joi.string()
            .required(),
        photo_room1: Joi.string()
            .required(),
        website: Joi.string(),
        facebook: Joi.string(),
        twitter: Joi.string(),
        instagram: Joi.string(),
        weekly_price: Joi.number()
            .required(),
        monthly_price: Joi.number()
            .required(),
        description: Joi.string()
    });

    try {
        await schema.validateAsync(request.body);

        const BCRYPT_SALT_ROUNDS = 12;
        const hashedPassword = await bcrypt.hash(password, BCRYPT_SALT_ROUNDS);
        const user = await db.saveHostUser(contactName, surname, email, hashedPassword,
            colivingName, apartments, rooms_count, roomType, facilities,
            website, facebook, instagram, twitter, photo_room1, street,
            zipcode, city, country, weekly_price, monthly_price, description);

        const token = jsonwebtoken.sign({
            id: user.id,
            role: 'host'
        }, config.token_password, {
            expiresIn: 86400
        });
        response.send({ token, ...user });
    } catch (e) {
        console.log(e.message);
        response.status(400).json(e.message);
    }
});

// Login de gestores
app.post('/loginHost', async (request, response) => {
    try {
        const email = request.body.email;
        const password = request.body.password;
        const user = await db.getHostUserByEmail(email);
        const samePassword = await bcrypt.compare(password, user.contact_password);
        if (!samePassword) {
            return response.status(403).send('Usuario o password incorrectos');
        }
        const { id, name, street, zip_code, city, country, apartments_count,
            rooms_count, room_type, weekly_price, monthly_price, poster, score, contact_name,
            contact_surname, website, facebook, instagram, twitter, description, contact_email } = user;

        const token = jsonwebtoken.sign({
            id: id,
            role: 'host'
        }, config.token_password, {
            expiresIn: 86400
        });
        response.json({
            token, id, name, street, zip_code, city, country, apartments_count,
            rooms_count, room_type, weekly_price, monthly_price, poster, score, contact_name,
            contact_surname, website, facebook, instagram, twitter, description, contact_email
        });
    } catch (e) {
        console.log(e.message);
        response.status(403).send({});
    }
});

// Login de gestores
app.post('/host/login', async (request, response) => {
    try {
        const email = request.body.email;
        const password = request.body.password;
        const user = await db.getHostUserByEmail(email);
        const id = user.id;
        const name = user.name;
        const surname = user.surname;
        const profession = user.profession;
        const country = user.country;

        const samePassword = await bcrypt.compare(password, user.password);
        if (!samePassword) {
            return response.status(403).send('Usuario o password incorrectos');
        }

        const token = jsonwebtoken.sign({
            id: id,
            role: 'host'
        }, config.token_password, {
            expiresIn: 86400
        });
        response.json({ token, id, name, surname, email, profession, country });
    } catch (e) {
        console.log(e.message);
        response.status(403).send({});
    }
});

// Devuelve todos los colivings
app.get('/colivings', async (request, response) => {
    let colivings = await db.getColivings();
    response.status(200).json(colivings);
});

// Búsqueda de colivings
// TODO Por ahora sólo busca por ciudad
app.post('/search/colivings', async (request, response) => {
    const city = request.body['city'];
    const colivings = await db.searchColivingsByCity(city);
    response.status(200).json(colivings);
});

// Obtiene la información de un coliving
app.get('/coliving/:id', async (request, response) => {
    const idColiving = request.params.id;
    const colivingInfo = await db.getColivingInfo(idColiving);
    response.status(200).json(colivingInfo);
});

// Obtiene la información de un apartamento
app.get('/apartments/:id_coliving/', async (request, response) => {
    const idColiving = request.params.id_coliving;
    const apartments = await db.getApartmentsByColivingId(idColiving);
    response.status(200).json(apartments);
});

// Obtiene el perfil de un coliver
app.get('/user/profile', async (request, response) => {
    const BEARER_END = 7;
    const authorizationHeader = request.headers.authorization;
    const requestToken = authorizationHeader.slice(BEARER_END);
    if (!requestToken) {
        return response.status(401).send("No hay token");
    }

    jsonwebtoken.verify(requestToken, config.token_password, async function (error, tokenInfo) {
        if (error)
            return response.status(401).send("Authentication failed");
        if (tokenInfo.role !== 'coliver') {
            return response.status(403).send("Rol no válido");
        }

        const idColiver = tokenInfo.id;
        const coliver = await db.getColiverById(idColiver);
        response.status(200).json(coliver);
    });
});

// Edita el perfil de un coliver
app.put('/user/profile/edit', async (request, response) => {
    const BEARER_END = 7;
    const authorizationHeader = request.headers.authorization;
    const requestToken = authorizationHeader.slice(BEARER_END);
    if (!requestToken) {
        return response.status(401).send("No hay token");
    }

    jsonwebtoken.verify(requestToken, config.token_password, async function (error, tokenInfo) {
        if (error)
            return response.status(401).send("Autenticación fallida");

        if (tokenInfo.role !== 'coliver') {
            return response.status(403).send("Rol no válido");
        }

        const name = request.body.name;
        const surname = request.body.surname;
        const email = request.body.email;
        const password = request.body.password;
        const profession = request.body.profession;
        const country = request.body.country;
        const id = tokenInfo.id;

        if (typeof password !== 'undefined') {
            const BCRYPT_SALT_ROUNDS = 12;
            const hashedPassword = await bcrypt.hash(password, BCRYPT_SALT_ROUNDS);
            await db.editColiverWithPassword(id, name, surname, email, hashedPassword, profession, country);
        } else {
            await db.editColiver(id, name, surname, email, profession, country);
        }
        response.status(200).json();
    });
});

// Valora una reserva
app.post('/user/reservation/rate', async (request, response) => {
    const BEARER_END = 7;
    const authorizationHeader = request.headers.authorization;
    const requestToken = authorizationHeader.slice(BEARER_END);
    if (!requestToken) {
        return response.status(401).send("No hay token");
    }

    jsonwebtoken.verify(requestToken, config.token_password, async function (error, tokenInfo) {
        if (error)
            return response.status(401).send("Autenticación fallida");

        if (tokenInfo.role !== 'coliver') {
            return response.status(403).send("Rol no válido");
        }

        const idReservation = request.body.idReservation;
        const score = request.body.score;
        const id = tokenInfo.id;
        await db.rateReservation(idReservation, score, id);
        return response.status(200).json();
    });
});

// Obtiene la información de los apartamentos donde se ha alojado un coliver
app.get('/user/reservations', async (request, response) => {
    const BEARER_END = 7;
    const authorizationHeader = request.headers.authorization;
    const requestToken = authorizationHeader.slice(BEARER_END);
    if (!requestToken) {
        return response.status(401).send("No hay token");
    }

    jsonwebtoken.verify(requestToken, config.token_password, async function (error, tokenInfo) {
        if (error)
            return response.status(401).send("Autenticación fallida");

        if (tokenInfo.role !== 'coliver') {
            return response.status(403).send("Rol no válido");
        }

        const id = tokenInfo.id;
        const reservations = await db.getReservationsByColiverId(id);
        return response.status(200).json(reservations);
    });
});

// Valora un coliver
app.post('/user/coliver/rate', async (request, response) => {
    const BEARER_END = 7;
    const authorizationHeader = request.headers.authorization;
    const requestToken = authorizationHeader.slice(BEARER_END);
    if (!requestToken) {
        return response.status(401).send("No hay token");
    }

    jsonwebtoken.verify(requestToken, config.token_password, async function (error, tokenInfo) {
        if (error)
            return response.status(401).send("Autenticación fallida");

        if (tokenInfo.role !== 'coliver') {
            return response.status(403).send("Rol no válido");
        }

        const scoredEmail = request.body.scored;
        const score = request.body.score;
        const scorer = tokenInfo.id;

        try {
            await db.rateColiver(scorer, scoredEmail, score);
            response.status(200).json();
        } catch (e) {
            response.status(500).json("Error al valorar el coliver");
        }
    });
});

// Registra una reserva
app.post('/user/reservation/add', async (request, response) => {
    const BEARER_END = 7;
    const authorizationHeader = request.headers.authorization;
    const requestToken = authorizationHeader.slice(BEARER_END);
    if (!requestToken) {
        return response.status(401).send("No hay token");
    }

    jsonwebtoken.verify(requestToken, config.token_password, async function (error, tokenInfo) {
        if (error)
            return response.status(401).send("Autenticación fallida");

        if (tokenInfo.role !== 'coliver') {
            return response.status(403).send("Rol no válido");
        }

        const bookingNumber = randtoken.generate(6, "1234567890");
        const entryDate = request.body.EntryWeek;
        const departureDate = request.body.DepartureWeek;
        let duration = (new Date(departureDate) - new Date(entryDate)) / (1000 * 60 * 60 * 24 * 7);
        const idColiver = tokenInfo.id;
        const idApartment = request.body.apartment.id;
        const price = Math.round(duration * await db.getColivingWeeklyPriceByAparmentId(idApartment) * 100) / 100;
        await db.addReservation(bookingNumber, price, entryDate, departureDate, idColiver, idApartment);
        return response.status(200).json();
    });
});

// Obtiene las reservas de un usuario
app.get('/user/reservation/colivings', async (request, response) => {
    const BEARER_END = 7;
    const authorizationHeader = request.headers.authorization;
    const requestToken = authorizationHeader.slice(BEARER_END);
    if (!requestToken) {
        return response.status(401).send("No hay token");
    }

    jsonwebtoken.verify(requestToken, config.token_password, async function (error, tokenInfo) {
        if (error)
            return response.status(401).send("Autenticación fallida");

        if (tokenInfo.role !== 'coliver') {
            return response.status(403).send("Rol no válido");
        }

        const id = tokenInfo.id;
        const colivings = await db.getColivingsWithReservation(id);
        response.status(200).json(services)
    });
});

// Obtiene los servicios de un usuario
app.get('/user/services', async (request, response) => {
    const BEARER_END = 7;
    const authorizationHeader = request.headers.authorization;
    const requestToken = authorizationHeader.slice(BEARER_END);
    if (!requestToken) {
        return response.status(401).send("No hay token");
    }

    jsonwebtoken.verify(requestToken, config.token_password, async function (error, tokenInfo) {
        if (error)
            return response.status(401).send("Autenticación fallida");

        if (tokenInfo.role !== 'coliver') {
            return response.status(403).send("Rol no válido");
        }

        const id = tokenInfo.id;
        const services = await db.getServicesByColiverId(id);
        response.status(200).json(services)
    });
});

// Obtiene el perfil de un gestor
app.get('/host/profile', async (request, response) => {
    const BEARER_END = 7;
    const authorizationHeader = request.headers.authorization;
    const requestToken = authorizationHeader.slice(BEARER_END);
    if (!requestToken) {
        return response.status(401).send("No hay token");
    }

    jsonwebtoken.verify(requestToken, config.token_password, async function (error, tokenInfo) {
        if (error)
            return response.status(401).send("Autenticación fallida");

        if (tokenInfo.role !== 'host') {
            return response.status(403).send("Rol no válido");
        }

        const id = tokenInfo.id;
        const host = await db.getHostById(id);
        response.status(200).json(host);
    });
});

// Editar el perfil de un gestor
app.put('/host/dashboard/profile/edit', async (request, response) => {
    const BEARER_END = 7;
    const authorizationHeader = request.headers.authorization;
    const requestToken = authorizationHeader.slice(BEARER_END);
    if (!requestToken) {
        return response.status(401).send("No hay token");
    }

    jsonwebtoken.verify(requestToken, config.token_password, async function (error, tokenInfo) {
        if (error)
            return response.status(401).send("Autenticación fallida");

        if (tokenInfo.role !== 'host') {
            return response.status(403).send("Rol no válido");
        }

        const body = request.body;

        try {
            await db.editHost(body);
            response.status(200).json();
        }catch (e) {
            console.log(e);
            response.status(400).json();
        }
    });
});

// Publica un apartamento
app.post('/host/add/apartment', async (request, response) => {
    const BEARER_END = 7;
    const authorizationHeader = request.headers.authorization;
    const requestToken = authorizationHeader.slice(BEARER_END);
    if (!requestToken) {
        return response.status(401).send("No hay token");
    }

    jsonwebtoken.verify(requestToken, config.token_password, async function (error, tokenInfo) {
        if (error)
            return response.status(401).send("Autenticación fallida");

        if (tokenInfo.role !== 'host') {
            return response.status(403).send("Rol no válido");
        }

        const roomsCount = request.body.rooms_count;
        const photoRoom1 = request.body.photo_room1;
        const id = tokenInfo.id;

        await db.addApartment(id, roomsCount, photoRoom1);
        response.status(200).json();
    });
});

// Elimina un apartamento
app.delete('/host/apartment/delete', async (request, response) => {
    const BEARER_END = 7;
    const authorizationHeader = request.headers.authorization;
    const requestToken = authorizationHeader.slice(BEARER_END);
    if (!requestToken) {
        return response.status(401).send("No hay token");
    }

    jsonwebtoken.verify(requestToken, config.token_password, async function (error, tokenInfo) {
        if (error)
            return response.status(401).send("Autenticación fallida");

        if (tokenInfo.role !== 'host') {
            return response.status(403).send("Rol no válido");
        }

        const id = request.body.id;
        const idHost = tokenInfo.id;
        await db.deleteApartment(id, idHost);
        response.status(200).json();
    });
});

// Modifica un apartamento
app.put('/host/apartment/edit', async (request, response) => {
    const BEARER_END = 7;
    const authorizationHeader = request.headers.authorization;
    const requestToken = authorizationHeader.slice(BEARER_END);
    if (!requestToken) {
        return response.status(401).send("No hay token");
    }

    jsonwebtoken.verify(requestToken, config.token_password, async function (error, tokenInfo) {
        if (error)
            return response.status(401).send("Autenticación fallida");

        if (tokenInfo.role !== 'host') {
            return response.status(403).send("Rol no válido");
        }

        const roomsCount = request.body.rooms_count;
        const photoRoom1 = request.body.photo_room1;
        const id = request.body.id;
        const idHost = tokenInfo.id;

        await db.addApartment(id, roomsCount, photoRoom1, idHost);
        response.status(200).json();
    });
});

// Obtiene los servicios de un gestor
app.get('/host/coliving/services', async (request, response) => {
    const BEARER_END = 7;
    const authorizationHeader = request.headers.authorization;
    const requestToken = authorizationHeader.slice(BEARER_END);
    if (!requestToken) {
        return response.status(401).send("No hay token");
    }

    jsonwebtoken.verify(requestToken, config.token_password, async function (error, tokenInfo) {
        if (error)
            return response.status(401).send("Autenticación fallida");

        if (tokenInfo.role !== 'host') {
            return response.status(403).send("Rol no válido");
        }

        const id = tokenInfo.id;
        const services = await db.getServicesByColivingId(id);
        response.status(200).json(services)
    });
});

// Obtiene las reservas de un gestor
app.get('/host/coliving/reservations', async (request, response) => {
    const BEARER_END = 7;
    const authorizationHeader = request.headers.authorization;
    const requestToken = authorizationHeader.slice(BEARER_END);
    if (!requestToken) {
        return response.status(401).send("No hay token");
    }

    jsonwebtoken.verify(requestToken, config.token_password, async function (error, tokenInfo) {
        if (error)
            return response.status(401).send("Autenticación fallida");

        if (tokenInfo.role !== 'host') {
            return response.status(403).send("Rol no válido");
        }

        const id = tokenInfo.id;
        console.log(id);
        const reservations = await db.getReservationsByColivingId(id);
        console.log(reservations);
        response.status(200).json(reservations)
    });
});

// Obtiene los servicios registrados
app.get('/services', async(request, response) => {
   const services = await db.getServices();
   response.status(200).json(services);
});

// Envia un mensaje (formulario de contacto)
app.post('/contact', function (req, res, next) {
    const BEARER_END = 7;
    const authorizationHeader = request.headers.authorization;
    const requestToken = authorizationHeader.slice(BEARER_END);
    if (!requestToken) {
        return response.status(401).send("No hay token");
    }

    const name = req.body.name;
    const email = req.body.email;
    const message = req.body.message;

    sgMail.setApiKey(process.env.SENDGRID_API_KEY);
    const msg = {
        to: 'colivingxt@gmail.com',
        from: email,
        subject: name,
        html: message,
    };
    sgMail.send(msg);

    res.send('OK');
});

app.listen(port, () => {
    console.log(`API disponible en: http://localhost:${port}`)
});
