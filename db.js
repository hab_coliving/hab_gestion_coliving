const { Pool } = require('pg');
const moment = require('moment');
const pool = new Pool({
    host: 'localhost',
    user: 'postgres',
    max: 20,
    port: 5432,
    password: 'trini',
    database: 'colivingsxt'
});

// Registra un coliver
const saveUser = async (name, surname, email, password, country, profession) => {
    let query = `select * from colivers where email='${email}' `;
    let result = await pool.query(query);
    if (result.rows.length !== 0) {

        throw Error('Usuario existente');
    }

    query = `insert into colivers (name, surname, email, password, country, profession) values ('${name}','${surname} + 
            ','${email}','${password}','${country}','${profession}')`;
    result = await pool.query(query);
    if (result.rowCount !== 1) {
        throw Error('Usuario no guardado');
    }

    query = `select id from colivers where email='${email}'`;
    result = await pool.query(query);
    return result.rows[0].id;
};

// Registra un gestor
const saveHostUser = async (contactName, surname, email, hashedPassword, colivingName, apartments, rooms_count,
                            roomType, facilities, website, facebook, instagram, twitter, photo_room1, street, zipcode,
                            city, country, weekly_price, monthly_price, description) => {
    let query = `select * from colivings where contact_email='${email}' and name = '${colivingName}' `;
    let result = await pool.query(query);
    if (result.rows.length !== 0) {
        throw Error('Usuario existente');
    }

    query = `insert into colivings 
        (contact_name, contact_surname, contact_email, contact_password, name, 
            apartments_count, rooms_count, room_type, website, facebook, instagram, twitter, zip_code, street, 
            city, country, poster, weekly_price, monthly_price, description) values 
        ('${contactName}','${surname}','${email}','${hashedPassword}','${colivingName}',
        '${apartments}','${rooms_count}','${roomType}','${website}','${facebook}','${instagram}','${twitter}','${zipcode}','${street}',
        '${city}','${country}','${photo_room1}','${weekly_price}','${monthly_price}','${description}')
         returning *`;
    result = await pool.query(query);
    if (result.rowCount !== 1) {
        throw Error('Usuario no guardado');
    }
    const idColiving = result.rows[0].id;

    // Relaciona los servicios con el coliving registrado
    for (const facility of facilities) {
        const idService = await getIdService(facility);
        console.log(idService);
        query = 'insert into service_coliving (id_service, id_coliving) values ($1, $2)';
        await pool.query(query, [idService, idColiving]);
    }

    return result.rows[0];
};

const getIdService = async (name) => {
  const query = 'select id from services where name = $1';
  const result = await pool.query(query, [name]);
  return result.rows[0].id;
};

// Obtiene un coliver a partir de su email
const getUserByEmail = async (email) => {
    let query = `select * from colivers where email='${email}' `;
    //Faltan todos os datos para k o facer login se lle pasen ó perfil
    const result = await pool.query(query);
    if (result.rows.length === 0) {
        throw Error('Usuario incorrecto');
    }
    return result.rows[0];
};

// Obtiene un gestor a partir de su email
const getHostUserByEmail = async (email) => {
    let query = `select * from colivings where contact_email='${email}' `;
    const result = await pool.query(query);

    if (result.rows.length === 0) {
        throw Error('Usuario incorrecto');
    }
    return result.rows[0];
};

// Obtiene todos los colivings
const getColivings = async () => {
    const query = 'select * from colivings';
    const result = await pool.query(query);
    return result.rows;
};

// Obtiene la información de un coliving
const getColivingInfo = async (id) => {
    const query = 'select * from colivings where id = $1';
    const result = await pool.query(query, [id]);
    return result.rows[0];
};

// Obtiene los apartamentos de un coliver
const getApartmentsByColivingId = async (idColiving) => {
    const query = 'select * from apartments where id_coliving = $1';
    const result = await pool.query(query, [idColiving]);
    return result.rows;
};

// Busca colivings según los criterios
const searchColivingsByCity = async (city) => {
    const query = 'select * from colivings where lower(city) LIKE lower($1)';
    const result = await pool.query(query, [city]);
    return result.rows;
};

// Obtiene los colivers de una misma profesión
const getColiversByProfession = async (profession) => {
    const query = 'select * from colivers where profession = $1';
    const result = await pool.query(query, [profession]);
    return result.rows;
};

// Info de un coliver
const getColiverById = async (id) => {
    const query = 'select * from colivers where id = $1';
    const results = await pool.query(query, [id]);
    return results.rows;
};

// Modifica el perfil de un coliver
const editColiver = async (id, name, surname, email, profession, country) => {
    const query = 'update colivers set name = $2, surname = $3, email = $4, profession = $5, country = $6 ' +
        'where id = $1';
    const result = await pool.query(query, [id, name, surname, email, profession, country]);
    return result.rows;
};

// Modifica el perfil de un coliver, incluyendo la contraseña
const editColiverWithPassword = async (id, name, surname, email, password, profession, country) => {
    const query = 'update colivers set name = $2, surname = $3, email = $4, password = $5, profession = $6, country = $7 ' +
        'where id = $1';
    const result = await pool.query(query, [id, name, surname, email, password, profession, country]);
    return result.rows;
};

// Valora a un coliver
const rateColiver = async (id_coliver_scorer, scoredEmail, score) => {
    const scoredUser = await getUserByEmail(scoredEmail);
    const query = 'insert into colivers_scoring (id_coliver_scorer, id_coliver_scored, score) values ($1, $2, $3)';
    await pool.query(query, [id_coliver_scorer, scoredUser.id, score]);
};

// Valora una reserva
const rateReservation = async (idReservation, rating, idColiver) => {
    const queryReservation = 'update reservations set score = $1 where id = $2 and id_coliver = $3';
    await pool.query(queryReservation, [rating, idReservation, idColiver]);
};

// Obtiene las reservas de un coliver
const getReservationsByColiverId = async (id) => {
    const query = 'select apartments.*, reservations.id as reservationid, reservations.entry_date as entrydate from reservations, apartments ' +
        'where reservations.id_apartment = apartments.id and reservations.id_coliver = $1';
    const result = await pool.query(query, [id]);
    return result.rows;
};

// Obtiene los servicios de un coliver
const getServicesByColiverId = async (id) => {
    const query = 'select distinct(services.name) from services, service_coliving, reservations, apartments where services.id = service_coliving.id_service ' +
        ' and service_coliving.id_coliving = apartments.id_coliving and reservations.id_apartment = apartments.id and reservations.id_coliver = $1';
    const result = await pool.query(query, [id]);
    return result.rows;
};

// Obtiene la información de un gestor
const getHostById = async (id) => {
    const query = 'select * from colivings where id = $1';
    const results = await pool.query(query, [id]);
    return results.rows;
};

// Editar el perfil de un gestor
const editHost = async (data) => {
    console.log(data.contact_name);
    const query = 'update colivings set contact_name = $1, contact_surname = $2, contact_email = $3, name = $4, street = $5,' +
    ' zip_code = $6, city = $7, country= $8, apartments_count = $9, rooms_count = $10, room_type = $11, weekly_price= $12,' +
    ' monthly_price = $13, website = $14, facebook = $15, instagram = $16, twitter = $17, poster = $18, description = $19 where id = $20';
    await pool.query(query, [
        data.contact_name, data.contact_surname, data.contact_email, data.name, data.street, data.zip_code, data.city, data.country,
        data.apartments_count, data.rooms_count, data.room_type, data.weekly_price, data.monthly_price, data.website, 
        data.facebook, data.instagram, data.twitter, data.poster, data.description, data.id]);
};

// Añade un apartamento
const addApartment = async (idColiving, rooms_count, photo_room1) => {
    const query = 'insert into apartments (id_coliving, rooms_count, photo_room1) values ($1, $2, $3)';
    await pool.query(query, [idColiving, rooms_count, photo_room1]);
};

// Elimina un apartamento
const deleteApartment = async (id, idHost) => {
    const query = 'delete from apartments where id = $1 and id_coliving = $2';
    await pool.query(query, [id, idHost]);
};

// Modifica una apartamento
const editApartment = async (id, rooms_count, photo_room1, idHost) => {
    const query = 'update apartments set rooms_count = $1, photo_room1 = $2 where id = $3 and id_coliving = $4';
    await pool.query(query, [rooms_count, photo_room1, id, idHost]);
};

// Registra una reserva
const addReservation = async (bookingNumber, price, entryDate, departureDate, idColiver, idApartment) => {
    const query = 'insert into reservations (booking_number, price, entry_date, departure_date, id_coliver, id_apartment) ' +
    ' values ($1, $2, $3, $4, $5, $6)';
    await pool.query(query, [bookingNumber, price, entryDate, departureDate, idColiver, idApartment]);
};

// Obtiene el precio semanal de un apartamento
const getColivingWeeklyPriceByAparmentId = async (idApartment) => {
    let query = 'select id_coliving from apartments where id = $1';
    let result = await pool.query(query, [idApartment]);
    const idColiving = result.rows[0].id_coliving;

    query = 'select weekly_price from colivings where id = $1';
    result = await pool.query(query, [idColiving]);
    return result.rows[0].weekly_price;
};

// Obtiene los servicios de un coliving
const getServicesByColivingId = async (id) => {
    const query = 'select services.* from services, service_coliving where ' +
        'services.id = service_coliving.id_service and service_coliving.id_coliving = $1';
    const result = await pool.query(query, [id]);
    return result.rows;
};

// Obtiene las reservas de un coliving
const getReservationsByColivingId = async (id) => {
    const query = 'select reservations.* from reservations, apartments where ' +
        'reservations.id_apartment = apartments.id and ' +
        'apartments.id_coliving = $1;';
    const result = await pool.query(query, [id]);
    return result.rows;
};

// Obtiene los servicios registrados
const getServices = async () => {
    const query = 'select * from services';
    const result = await pool.query(query);
    return result.rows;
};

module.exports = {
    saveUser,
    getUserByEmail,
    getColivings,
    getColivingInfo,
    getApartmentsByColivingId,
    searchColivingsByCity,
    getColiversByProfession,
    saveHostUser,
    getHostUserByEmail,
    getColiverById,
    editColiver,
    editColiverWithPassword,
    rateColiver,
    rateReservation,
    getReservationsByColiverId,
    getServicesByColiverId,
    getHostById,
    editHost,
    addApartment,
    deleteApartment,
    editApartment,
    addReservation,
    getColivingWeeklyPriceByAparmentId,
    getServicesByColivingId,
    getReservationsByColivingId,
    getServices
};